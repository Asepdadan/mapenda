<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengumumenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengumuman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('judul', 255);
            $table->string('judul_slug', 255);
            $table->text('isi');
            $table->integer('file_id')->unsigned();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');;
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengumuman');
    }
}
