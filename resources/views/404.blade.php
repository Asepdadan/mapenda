    
@extends('layouts.front')

@section('content')
	<section id="error" class="container text-center" style="margin-top:27px;margin-bottom:300px;">
        <h1>404, Halaman Tidak Ditemukan</h1>
        <p>Halaman yang anda kunjungi mungkin sudah rusak/tidak ada.</p>
        <a class="btn btn-primary" href="javascript:;" onclick="back()">Kembali Ke halaman sebelumnya</a>
    </section>
@endsection


@section('javascript')
<script>
	function back(){
		window.history.back()
	}
</script>
@endsection
