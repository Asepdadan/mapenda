@extends('layouts.front')

@section('title', ' HOme')


@section('content')
    <div class="center" style="margin-top: 10px;">
        <h2>Beranda</h2>
        <p class="lead">Informasi Seputar Pendidikan Di bawah Kewenangan Departemen Agama Kabupaten Garut</p>
    </div>

    <div class="container">
        <div class="center">
            <!-- <div class="informasi-running">
                <div class="text-informasi-running">
                    <p class="text-limit">Lorem ipsum est non veniam sint tempor in ut in tempor dolor nostrud magna irure excepteur voluptate commodo ea culpa qui occaecat.</p>
                </div>
                <div class="text-informasi-running">
                    <p class="text-limit">Lorem ipsum est non veniam sint tempor in ut in tempor dolor nostrud magna irure excepteur voluptate commodo ea culpa qui occaecat.</p>
                </div>
                <div class="text-informasi-running">
                    <p class="text-limit">Lorem ipsum est non veniam sint tempor in ut in tempor dolor nostrud magna irure excepteur voluptate commodo ea culpa qui occaecat.</p>
                </div>
                <div class="text-informasi-running">
                    <p class="text-limit">Lorem ipsum est non veniam sint tempor in ut in tempor dolor nostrud magna irure excepteur voluptate commodo ea culpa qui occaecat.</p>
                </div>
                <div class="text-informasi-running">
                    <p class="text-limit">Lorem ipsum est non veniam sint tempor in ut in tempor dolor nostrud magna irure excepteur voluptate commodo ea culpa qui occaecat.</p>
                </div>
                <div class="text-informasi-running">
                    <p class="text-limit">Lorem ipsum est non veniam sint tempor in ut in tempor dolor nostrud magna irure excepteur voluptate commodo ea culpa qui occaecat.</p>
                </div>
            </div> -->
            <div class="marquee">
              <div>
               <!--  <span>You spin me right round, baby. Like a record, baby.</span>
                <span>You spin me right round, baby. Like a record, baby.</span> -->
                <span>Magna deserunt aliquip in id pariatur quis incididunt ut tempor in eiusmod ea deserunt cupidatat sed do tempor deserunt cupidatat in..</span>
                <span>Laborum sunt amet mollit est duis qui ad non aliquip consequat sint aliquip magna duis magna nostrud ut amet.</span>
                <span>Eu enim eu pariatur esse.</span>
                <span>Officia duis in et elit in ullamco qui ullamco exercitation exercitation excepteur ut reprehenderit mollit commodo sed dolore.</span>
              </div>
            </div>
        </div>
    </div>
    

    <section id="main-slider" class="no-margin" style="max-height: 750px;">
        <div class="col-md-9 col-xs-12">
            <div class="panel panel-danger" style="margin-top:10px;border:3px solid #f4f5f7;">
                <div class="panel-heading" style="background: #46874e; color: white;border-radius: 8px;">
                    <h3 class="panel-title"><i class="fa fa-info-circle"></i> Pengumuman</h3>
                </div>
                <div class="panel-body" style="background:white;">
                     <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 ">
                               <div class="tab-wrap"> 
                                    <div class="media">
                                        <div class="parrent pull-left">
                                            <ul class="nav nav-tabs nav-stacked">
                                                <li class="active">
                                                    <a href="#tab1" data-toggle="tab" class="analistic-01">Pengumuman Terkini</a>
                                                </li>
                                                <!-- <li class="">
                                                    <a href="#tab2" data-toggle="tab" class="analistic-02">
                                                        
                                                    </a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab3" data-toggle="tab" class="tehnical">Predefine Layout</a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab4" data-toggle="tab" class="tehnical">Our Philosopy</a>
                                                </li>
                                                <li class="">
                                                    <a href="#tab5" data-toggle="tab" class="tehnical">What We Do?</a>
                                                </li>  -->
                                            </ul>
                                        </div>

                                        <div class="parrent media-body">
                                            <div class="tab-content">
                                                <div class="tab-pane fade active in" id="tab1">
                                                    <div class="media">
                                                       <div class="pull-left">
                                                            <img class="img-responsive" src="{{asset('assets/images/tab2.png')}}">
                                                        </div>
                                                        <div class="media-body">
                                                            <p class="text-judul-berita"> UNDANGAN PENGUATAN IMPLEMENTASI KURIKULUM 2013 TINGKAT RA TAHUN PELAJARAN 2018/2019</p>
                                                            <p class="text-sm-left"><i class="fa fa-calendar"></i> 10 Desember 2018  <i class="fa fa-clock-o"></i> 09:12 WIB</p>
                                                            <br>

                                                             <p class="text-justify">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use. Lorem ipsum reprehenderit aute et eiusmod sint ea reprehenderit aliqua officia dolore consequat duis ea ut aute consequat aliquip laboris reprehenderit ut elit velit quis ut sit ut ad ad dolore ad laborum tempor deserunt laborum dolore pariatur aliquip deserunt et nulla eu commodo qui ut adipisicing eiusmod sed et officia pariatur dolore ut reprehenderit ut dolor est sit dolor tempor laboris dolore in enim laboris elit laborum et dolore in aute ut dolor aliquip dolore aute aliquip et consectetur in ex dolore pariatur dolor irure velit mollit voluptate quis ad ut ea ea minim voluptate ullamco dolor deserunt sit esse mollit ut irure dolor mollit reprehenderit commodo non ad consectetur in exercitation consectetur fugiat proident veniam enim ad ex magna in non dolor nulla ex proident aliqua ullamco id cupidatat dolore in do aute esse proident fugiat consequat nisi tempor amet in labore in exercitation et qui tempor mollit mollit eiusmod excepteur veniam excepteur nisi et ea labore aute qui reprehenderit ut dolor qui magna cillum incididunt dolor fugiat elit dolor amet incididunt sit laboris anim proident est tempor dolor ullamco et cillum incididunt incididunt officia aute quis ex consequat ea nisi proident nostrud fugiat officia cillum adipisicing amet incididunt et enim ea ex voluptate ut.</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                 <div class="tab-pane fade" id="tab2">
                                                    <div class="media">
                                                       <div class="pull-left">
                                                            <img class="img-responsive" src="{{asset('assets/images/tab1.png')}}">
                                                        </div>
                                                        <div class="media-body">
                                                             <h2>Adipisicing elit</h2>
                                                             <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use.
                                                             </p>
                                                        </div>
                                                    </div>
                                                 </div>

                                                 <div class="tab-pane fade" id="tab3">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.
                                                        Occaecat nulla consequat aute esse dolor tempor adipisicing ea proident ea amet elit tempor nostrud id nulla est dolor voluptate ullamco aliquip velit velit qui voluptate sint aute occaecat occaecat sint eiusmod cupidatat eiusmod pariatur in eiusmod dolor laboris nisi est consequat deserunt officia dolore dolor aliqua consequat fugiat ea occaecat aliquip do dolor quis enim labore dolor nostrud ut ut labore nulla aliquip consectetur labore aliqua voluptate ut culpa laborum irure veniam quis nostrud aliquip anim sed occaecat sed magna et reprehenderit veniam dolor est culpa nulla tempor dolore reprehenderit quis incididunt irure proident enim ut consectetur mollit irure culpa ut duis excepteur qui magna in aliquip ullamco dolor in duis velit amet excepteur anim ullamco dolor culpa et laborum incididunt ex irure incididunt sint do in aliquip ullamco proident fugiat deserunt excepteur dolore occaecat eiusmod sed eu nostrud commodo in exercitation enim in cillum non dolore excepteur nisi aute sit exercitation officia in sunt esse aliquip tempor anim quis ut occaecat ut mollit esse duis nisi dolore enim adipisicing nostrud velit amet aliqua anim officia elit tempor dolore elit officia fugiat ut ad exercitation commodo in aliqua sunt cupidatat consequat cupidatat laboris veniam est aliquip in irure nostrud nisi nulla ex ex adipisicing magna in duis esse ut laborum veniam ullamco voluptate commodo duis nulla cupidatat quis nostrud dolor exercitation in elit cillum proident proident laborum ut laboris dolore in amet commodo ea.
                                                    </p>
                                                 </div>
                                                 
                                                 <div class="tab-pane fade" id="tab4">
                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words</p>
                                                 </div>

                                                 <div class="tab-pane fade" id="tab5">
                                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures,</p>
                                                 </div>
                                            </div> <!--/.tab-content-->  
                                        </div> <!--/.media-body--> 
                                    </div> <!--/.media-->     
                                </div><!--/.tab-wrap-->               
                            </div><!--/.col-sm-6-->
                        </div><!--/.row-->
                    </div>
                </div>
            </div>
            <!-- <div class="carousel slide">
                <ol class="carousel-indicators">
                    <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                    <li data-target="#main-slider" data-slide-to="1"></li>
                    <li data-target="#main-slider" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">

                    <div class="item active" style="background-image: url(assets/images/slider/bg1.jpg);height:630px">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-6">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1">Lorem ipsum dolor sit amet consectetur adipisicing elit</h1>
                                        <h2 class="animation animated-item-2">Accusantium doloremque laudantium totam rem aperiam, eaque ipsa...</h2>
                                        <a class="btn-slide animation animated-item-3" href="#">Read More</a>
                                    </div>
                                </div>

                                <div class="col-sm-6 hidden-xs animation animated-item-4">
                                    <div class="slider-img">
                                        <img src="{{asset('assets/images/slider/img1.png')}}" height="500" >
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="item" style="background-image: url(assets/images/slider/bg2.jpg)">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-6">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1">Lorem ipsum dolor sit amet consectetur adipisicing elit</h1>
                                        <h2 class="animation animated-item-2">Accusantium doloremque laudantium totam rem aperiam, eaque ipsa...</h2>
                                        <a class="btn-slide animation animated-item-3" href="#">Read More</a>
                                    </div>
                                </div>

                                <div class="col-sm-6 hidden-xs animation animated-item-4">
                                    <div class="slider-img">
                                        <img src="{{asset('assets/images/slider/img2.png')}}" class="img-responsive">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="item" style="background-image: url(assets/images/slider/bg3.jpg)">
                        <div class="container">
                            <div class="row slide-margin">
                                <div class="col-sm-6">
                                    <div class="carousel-content">
                                        <h1 class="animation animated-item-1">Lorem ipsum dolor sit amet consectetur adipisicing elit</h1>
                                        <h2 class="animation animated-item-2">Accusantium doloremque laudantium totam rem aperiam, eaque ipsa...</h2>
                                        <a class="btn-slide animation animated-item-3" href="#">Read More</a>
                                    </div>
                                </div>
                                <div class="col-sm-6 hidden-xs animation animated-item-4">
                                    <div class="slider-img">
                                        <img src="{{asset('assets/images/slider/img3.png')}}" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
                <i class="fa fa-chevron-left"></i>
            </a>
            <a class="next hidden-xs" href="#main-slider" data-slide="next">
                <i class="fa fa-chevron-right"></i>
            </a> -->
        </div>

        <div class="col-md-3 col-xs-12 col-sm-12">
            <div class="panel panel-danger" style="margin-top:10px;">
                <div class="panel-heading" style="background: #46874e; color: white;border-radius: 8px;">
                    <h3 class="panel-title">Ketua Bidang Mapenda</h3>
                </div>
                <div class="panel-body" style="background:white;">
                    <div class="thumbnail">
                        <img src="https://upload.wikimedia.org/wikipedia/commons/6/63/Chris_Wray_official_photo.jpg" alt="" class="img-responsive">
                        <div class="caption">
                            <p class="text-center">H. Dr. Muhammad Badru Zaman, S.Tr.Kom, M.Kom</p>
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#main-slider-->
        
        
	
    <div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Berita Terpilih</h3>
		</div>
        <div class="panel-body">
            <div class="container-fluid">
                <div class="row">
					<div class="berita-running">
						<div>
		                  	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                        <div class="thumbnail">
		                            <img data-src="#" alt="">
		                            <div class="caption">
		                                <div class="panel panel-default">
		                                    <div class="panel-body div-berita">
		                                        <p class="text-judul-berita">
		                                            <a href="#">Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi </a>
		                                        </p>
		                                        <p class="text-berita">
		                                            Quis nulla quis in aliquip cillum aliqua sit mollit ut nostrud ut qui proident ullamco amet aliqua quis labore ut dolore aliqua ut ea ad consectetur consectetur nulla consectetur velit non labore exercitation adipisicing reprehenderit sit nulla ut irure non proident reprehenderit ullamco enim exercitation ut deserunt est duis ad velit do amet est reprehenderit dolor ea nostrud amet nulla ut laborum do do in dolor aute in incididunt ex reprehenderit dolor ut incididunt velit proident cupidatat mollit fugiat do enim elit cillum excepteur cillum labore dolore culpa dolore velit reprehenderit amet reprehenderit aliqua ex aliquip est eiusmod id dolore laboris in cupidatat ad eiusmod cupidatat excepteur et quis ad in irure sit mollit et duis nulla dolore minim aute ut elit aute enim adipisicing ut ut commodo est magna veniam nulla ut velit dolore laborum cupidatat officia.
		                                        </p>
		                                           
		                                        <div class="col-md-5 col-sm-5 col-xs-5 text-detail">
		                                            <i class="fa fa-eye"></i> 18000 K
		                                        </div>
		                                        <div class="col-md-7 col-sm-7 col-xs-7 text-detail text-right">
		                                            <i class="fa fa-calendar"></i> 18 Desember 2018 09:05 WIB
		                                        </div>
		                                    </div>
		                                </div>                                
		                            </div>
		                        </div>
		                    </div>
	                    </div>
						
						<div>
		                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                        <div class="thumbnail">
		                            <img data-src="#" alt="">
		                            <div class="caption">
		                                <div class="panel panel-default">
		                                    <div class="panel-body div-berita">
		                                        <p class="text-judul-berita">
		                                            <a href="#">Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi </a>
		                                        </p>
		                                        <p class="text-berita">
		                                            Quis nulla quis in aliquip cillum aliqua sit mollit ut nostrud ut qui proident ullamco amet aliqua quis labore ut dolore aliqua ut ea ad consectetur consectetur nulla consectetur velit non labore exercitation adipisicing reprehenderit sit nulla ut irure non proident reprehenderit ullamco enim exercitation ut deserunt est duis ad velit do amet est reprehenderit dolor ea nostrud amet nulla ut laborum do do in dolor aute in incididunt ex reprehenderit dolor ut incididunt velit proident cupidatat mollit fugiat do enim elit cillum excepteur cillum labore dolore culpa dolore velit reprehenderit amet reprehenderit aliqua ex aliquip est eiusmod id dolore laboris in cupidatat ad eiusmod cupidatat excepteur et quis ad in irure sit mollit et duis nulla dolore minim aute ut elit aute enim adipisicing ut ut commodo est magna veniam nulla ut velit dolore laborum cupidatat officia.
		                                        </p>
		                                           
		                                        <div class="col-md-5 col-sm-5 col-xs-5 text-detail">
		                                            <i class="fa fa-eye"></i> 18000 K
		                                        </div>
		                                        <div class="col-md-7 col-sm-7 col-xs-7 text-detail text-right">
		                                            <i class="fa fa-calendar"></i> 18 Desember 2018 09:05 WIB
		                                        </div>
		                                    </div>
		                                </div>                                
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                
		                <div>
	                    	<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                        <div class="thumbnail">
		                            <img data-src="#" alt="">
		                            <div class="caption">
		                                <div class="panel panel-default">
		                                    <div class="panel-body div-berita">
		                                        <p class="text-judul-berita">
		                                            <a href="#">Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi </a>
		                                        </p>
		                                        <p class="text-berita">
		                                            Quis nulla quis in aliquip cillum aliqua sit mollit ut nostrud ut qui proident ullamco amet aliqua quis labore ut dolore aliqua ut ea ad consectetur consectetur nulla consectetur velit non labore exercitation adipisicing reprehenderit sit nulla ut irure non proident reprehenderit ullamco enim exercitation ut deserunt est duis ad velit do amet est reprehenderit dolor ea nostrud amet nulla ut laborum do do in dolor aute in incididunt ex reprehenderit dolor ut incididunt velit proident cupidatat mollit fugiat do enim elit cillum excepteur cillum labore dolore culpa dolore velit reprehenderit amet reprehenderit aliqua ex aliquip est eiusmod id dolore laboris in cupidatat ad eiusmod cupidatat excepteur et quis ad in irure sit mollit et duis nulla dolore minim aute ut elit aute enim adipisicing ut ut commodo est magna veniam nulla ut velit dolore laborum cupidatat officia.
		                                        </p>
		                                           
		                                        <div class="col-md-5 col-sm-5 col-xs-5 text-detail">
		                                            <i class="fa fa-eye"></i> 18000 K
		                                        </div>
		                                        <div class="col-md-7 col-sm-7 col-xs-7 text-detail text-right">
		                                            <i class="fa fa-calendar"></i> 18 Desember 2018 09:05 WIB
		                                        </div>
		                                    </div>
		                                </div>                                
		                            </div>
		                        </div>
		                    </div>
		                </div>
		                
		                <div>
		                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
		                        <div class="thumbnail">
		                            <img data-src="#" alt="">
		                            <div class="caption">
		                                <div class="panel panel-default">
		                                    <div class="panel-body div-berita">
		                                        <p class="text-judul-berita">
		                                            <a href="#">Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi Akreditasi Ralat Undangan Sosialisasi </a>
		                                        </p>
		                                        <p class="text-berita">
		                                            Quis nulla quis in aliquip cillum aliqua sit mollit ut nostrud ut qui proident ullamco amet aliqua quis labore ut dolore aliqua ut ea ad consectetur consectetur nulla consectetur velit non labore exercitation adipisicing reprehenderit sit nulla ut irure non proident reprehenderit ullamco enim exercitation ut deserunt est duis ad velit do amet est reprehenderit dolor ea nostrud amet nulla ut laborum do do in dolor aute in incididunt ex reprehenderit dolor ut incididunt velit proident cupidatat mollit fugiat do enim elit cillum excepteur cillum labore dolore culpa dolore velit reprehenderit amet reprehenderit aliqua ex aliquip est eiusmod id dolore laboris in cupidatat ad eiusmod cupidatat excepteur et quis ad in irure sit mollit et duis nulla dolore minim aute ut elit aute enim adipisicing ut ut commodo est magna veniam nulla ut velit dolore laborum cupidatat officia.
		                                        </p>
		                                           
		                                        <div class="col-md-5 col-sm-5 col-xs-5 text-detail">
		                                            <i class="fa fa-eye"></i> 18000 K
		                                        </div>
		                                        <div class="col-md-7 col-sm-7 col-xs-7 text-detail text-right">
		                                            <i class="fa fa-calendar"></i> 18 Desember 2018 09:05 WIB
		                                        </div>
		                                    </div>
		                                </div>                                
		                            </div>
		                        </div>
		                    </div>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-sm-6 col-md-6 wow fadeInDown">
                <div class="skill">
                    <h2>Penilaian Pelayanan</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et  dolore magna aliqua.
                    </p>

                    <div class="progress-wrap">
                        <h3>Graphic Design</h3>
                        <div class="progress">
                          <div class="progress-bar  color1" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
                            <span class="bar-width">85%</span>
                          </div>

                        </div>
                    </div>

                    <div class="progress-wrap">
                        <h3>HTML</h3>
                        <div class="progress">
                          <div class="progress-bar color2" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 95%">
                           <span class="bar-width">95%</span>
                          </div>
                        </div>
                    </div>

                    <div class="progress-wrap">
                        <h3>CSS</h3>
                        <div class="progress">
                          <div class="progress-bar color3" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            <span class="bar-width">80%</span>
                          </div>
                        </div>
                    </div>

                    <div class="progress-wrap">
                        <h3>Wordpress</h3>
                        <div class="progress">
                          <div class="progress-bar color4" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                            <span class="bar-width">90%</span>
                          </div>
                        </div>
                    </div>
                </div>

            </div><!--/.col-sm-6-->
        </div>
    </div>

    <div class="panel">
		<div class="panel-heading">
			<h3 class="panel-title">Pegawai</h3>
		</div>
        <div class="panel-body">
            <div class="container">
			
            <div class="your-class">
                <div>
                    <img width="200" height="100" src="{{asset('assets/images/slider/img2.png')}}" alt="" class="img-circle">
                    <div class="caption">
                        <h3>BOS & Pembangunan</h3>
                        <p>
                            Est ut in aliquip deserunt proident ut sunt dolor occaecat aute adipisicing ullamco sunt est excepteur.
                        </p>
                    </div>
                </div>
                <div>
                    <img width="200" height="100" src="{{asset('assets/images/slider/img2.png')}}" alt="" class="img-circle">
                    <div class="caption">
                        <h3>Pemberkasan Sertifikasi</h3>
                        <p>
                            Est ut in aliquip deserunt proident ut sunt dolor occaecat aute adipisicing ullamco sunt est excepteur.
                        </p>
                    </div>
                </div>
                <div>
                    <img width="200" height="100" src="{{asset('assets/images/slider/img2.png')}}" alt="" class="img-circle">
                    <div class="caption">
                        <h3>Simpatika</h3>
                        <p>
                            Est ut in aliquip deserunt proident ut sunt dolor occaecat aute adipisicing ullamco sunt est excepteur.
                        </p>
                    </div>
                </div>
                <div>
                    <img width="200" height="100" src="{{asset('assets/images/slider/img2.png')}}" alt="" class="img-circle">
                    <div class="caption">
                        <h3>Simpatika</h3>
                        <p>
                            Est ut in aliquip deserunt proident ut sunt dolor occaecat aute adipisicing ullamco sunt est excepteur.
                        </p>
                    </div>
                </div>
                <div>
                    <img width="200" height="100" src="{{asset('assets/images/slider/img2.png')}}" alt="" class="img-rounded">
                    <div class="caption">
                        <h3>Simpatika</h3>
                        <p>
                            Est ut in aliquip deserunt proident ut sunt dolor occaecat aute adipisicing ullamco sunt est excepteur.
                        </p>
                    </div>
                </div>
                <div>
                    <img width="200" height="100" src="{{asset('assets/images/slider/img2.png')}}" alt="" class="img-rounded">
                    <div class="caption">
                        <h3>Simpatika</h3>
                        <p>
                            Est ut in aliquip deserunt proident ut sunt dolor occaecat aute adipisicing ullamco sunt est excepteur.
                        </p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@endsection
